#include "Optimisation.h"

#ifndef _INCLUDED_STRING_H_
#include <string>
#define _INCLUDED_STRING_H_
#endif

#ifndef _INCLUDED_TINYXML_2_H_
#include "tinyxml2.h"
#define _INCLUDED_TINYXML_2_H_
#endif

#ifndef _SPHAEROSIM_EXCEPTION_H_
#include "Exception.h"
#endif

#ifndef _SPHAEROSIM_SIMULATION_TESTSCENARIO_H_
#include "SimulationTestscenario.h"
#endif

#ifndef _SPHAEROSIM_SIMULATION_VTK_INPUT_H_
#include "SimulationVTKInput.h"
#endif

#ifndef _SPHAEROSIM_INTERPOLATION_H_
#include "Interpolation.h"
#endif

#ifndef _SPHAEROSIM_PROBE_H_
#include "Probe.h"
#endif

#ifndef _SPHAEROSIM_GROWTH_MODEL_H_
#include "GrowthModel.h"
#endif

#ifndef _SPHAEROSIM_NUCLEATION_MODEL_H_
#include "NucleationModel.h"
#endif

#ifndef _SPHAEROSIM_MATERIAL_H_
#include "Material.h"
#endif

#ifndef _SPHAEROSIM_HASH_VALUE_H_
#include "HashValue.h"
#endif

#ifndef _SPHAEROSIM_SIMULATION_COMSOL_INPUT_H_
#include "SimulationComsolInput.h"
#endif

#ifndef __DIRECTORY_H
#include "directory.h"
#endif

//my code for sa algorithm
// "SAAlgorithm.cpp": Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

// SA.cpp : Defines the entry point for the console application.
//
#include <iomanip>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>

using namespace tinyxml2;

namespace SphaeroSim {
	
	class Optimization
	{

	public:
		Optimization::Optimization()
		{
		}
		Optimization::~Optimization()
		{
		}

		//Read from Excel
		std::vector<std::vector<double>> Optimization::ReadfromExcel(std::string path_Element_Probes)
		{
			//output of reading Elemen_probes.csv for points_in_times and Crystdegree	
			std::vector<std::vector<double>> output_Element_probes;
			std::ifstream inFile;
			std::string Rel_Cryst_Deg;
			std::string points_in_times;
			std::string others;
			std::vector <double> RelCrysDeg;
			std::vector <double> PointsInTimes;
			int countstep = 0;

			//check wheather filename exists or not
			FILE *file;
			if (Directory::file_exists(path_Element_Probes) == true)
			{
				inFile.open(path_Element_Probes);

				if (!inFile.is_open())
				{
					std::cout << "NO FILE HAS BEEN OPENED" << std::endl;
					//getchar();
					RelCrysDeg.push_back(-1);
					PointsInTimes.push_back(-1);
					output_Element_probes[0][countstep] = RelCrysDeg.at(countstep);
					output_Element_probes[1][countstep] = PointsInTimes.at(countstep);
					//return output_Element_probes;
				}
				if (inFile.is_open())
					//read points in time and crystall degree from Element_probes.csv file
					while (!inFile.eof())
					{
						for (int index = 0; index < 5; index++)
						{
							getline(inFile, others, ';');
							getline(inFile, points_in_times, ';');
							getline(inFile, others, ';');
							getline(inFile, others, ';');
							getline(inFile, Rel_Cryst_Deg, ';');
						}
						RelCrysDeg.push_back(stod(Rel_Cryst_Deg));
						PointsInTimes.push_back(stod(points_in_times));
						output_Element_probes[0][countstep] = RelCrysDeg.at(countstep);
						output_Element_probes[1][countstep] = PointsInTimes.at(countstep);
						countstep++;
					}
				inFile.close();
			}
			else
			{
				RelCrysDeg.push_back(-1);
				PointsInTimes.push_back(-1);
				output_Element_probes[0][countstep] = RelCrysDeg.at(countstep);
				output_Element_probes[1][countstep] = PointsInTimes.at(countstep);
			}
			return output_Element_probes;
		}

		//Normal Rand
		double Optimization::NormalRand(double V0, double Zigma)
		{
			double adjacent_v0, u1, u2;
			srand((unsigned)time(0));
			u1 = double(rand() / 3);
			u2 = double(rand() / 2);
			adjacent_v0 = ((u1 * V0) + u2);
			while (adjacent_v0 > max_C_ath_ || adjacent_v0 < min_C_ath_)
				adjacent_v0 = ((u1 * V0) + u2);
			return adjacent_v0;

			//double NormalRand(double V0, double Zigma)
			//{
			//	double adjacent_v0, u1, u2;
			//	srand((unsigned)time(0));
			//	//u1 = double(rand());
			//	u1 = sqrt(-2 * log(double(rand())));
			//	//u2 = ((rand() / (double)RAND_MAX) * 2 - 1);
			//	u2 = cos(2 * 3.14159265 * ((rand() / (double)RAND_MAX) * 2 - 1));
			//	adjacent_v0 = V0 + Zigma * u1 * u2;
			//	while (adjacent_v0 > max_C_ath || adjacent_v0 < min_C_ath)
			//		adjacent_v0 = V0 + Zigma * u1 * u2;
			//	//system("pause");
			//	return adjacent_v0;
			//}
			//
			////Write to XML
			////void WritetoXML(string pathfile, double V0);
			//
			////cost function for 3 points randomely
			//
		}

		//Cost Function
		double Optimization::Cost_Function(std::vector<double> point_in_time, 
											std::vector<double> cryst_degree, 
											std::string target_value_file)
		{
			double Current_Cost = 0, mean_current = 0, mean_target = 0, sum_cryst_degree = 0, sum_point_n_time = 0;
			std::vector<double> point_in_time_target = ReadfromExcel(target_value_file)[0];
			std::vector<double> cryst_degree_target = ReadfromExcel(target_value_file)[1];

			for (auto index : cryst_degree)
				sum_cryst_degree += index;
			for (auto point_time : point_in_time)
				sum_point_n_time += point_time;
			mean_current = sum_cryst_degree / sum_point_n_time;

			for (auto index : point_in_time_target)
				sum_cryst_degree += index;
			for (auto point_time : point_in_time_target)
				sum_point_n_time += point_time;
			mean_target = sum_cryst_degree / sum_point_n_time;

			Current_Cost = (mean_target - mean_current) * (mean_target - mean_current);


			/*int RandStep;
			double Current_cost = 0;
			srand((unsigned)time(0));
			RandStep = (int)(rand() % (point_in_time + 1));
			while (RandStep > countstep || RandStep == countstep || RandStep < 0 || RandStep == 0)
			{
				RandStep = (int)(rand() % (countstep + 1));
			}
			for (int i = 0; i < 3; i++)
			{
				Current_cost += pow((*cryst_degree).at(RandStep), 2);
				int nextRandStep = RandStep + (int)(rand() % (countstep + 1));
				int backRandStep = RandStep - (int)(rand() % (countstep + 1));
				if (nextRandStep < countstep)
					RandStep = nextRandStep;
				else
					if (backRandStep > 0)
						RandStep = backRandStep;
			}*/
			return Current_Cost;
		}


		const size_t Optimization::GetProbeID() const
		{
			tinyxml2::XMLDocument file_handle(true, tinyxml2::PRESERVE_WHITESPACE);
			if (file_handle.LoadFile(optimization_.GetParameterFile().c_str()) != tinyxml2::XML_NO_ERROR)
				throw SphaeroSim::Exception("Parameter file",
					"ParameterFile is not found.");

			// load the root entry
			tinyxml2::XMLElement* root_element =
				file_handle.FirstChildElement("SphaeroSim");
			if (root_element == NULL)
				throw SphaeroSim::Exception("Value missing in parameter file",
					"The value <SphaeroSim> is missing.");

			// Simulation
			tinyxml2::XMLElement* cur_element =
				root_element->FirstChildElement("Simulation");
			if (cur_element == NULL)
				throw  SphaeroSim::Exception("Value missing in parameter file", "At least one <Simulation> must be defined.");

			//Probe_name
			tinyxml2::XMLElement* Probe_element =
				cur_element->FirstChildElement("Probe");
			if (Probe_element == NULL)
				throw SphaeroSim::Exception("Value missing in parameter file", "At least one <Probe> must be defined.");

			//Probe_id
			tinyxml2::XMLElement* probe_name_ =
				Probe_element->FirstChildElement("name");
			if (probe_name_ == NULL)
				throw SphaeroSim::Exception("Value missing in parameter file", "At least one <C_ath> must be defined.");
			const std::string probeid_str = probe_name_->GetText();
			const size_t probe_id = probeid_str.at((probeid_str.find("p") + 1));
			return probe_id;
		}

		const std::string Optimization::GetTargetValueFile() const
		{
			//the address take here in hadcode (by hand)
			std::string filename = "DSCScenario.csv";
			FILE *file;
			if (Directory::file_exists(filename) == true)
			{
				file = fopen(filename.c_str(), "a");
				if (file == NULL) 
				{
					printf("  -> Error: Could not open '%s'\n", filename.c_str());
					return;
				}
				else
					return filename;
			}
		}

		void Optimization::GetElementProbeResults(std::string probe_Name,
			std::string filename_str,
			std::vector<double>* points_in_time,
			std::vector<double>* cryst_degree)
		{
			cryst_degree = &ReadfromExcel(filename_str)[0];
			points_in_time = &ReadfromExcel(filename_str)[1];
		}

		const double Optimization::GetOptimisationQuality(const std::string target_value_file, 
			std::vector<double> points_in_time, 
			std::vector<double> cryst_degree)
		{
			
			double optimisation_quality;
			/*int RandStep;
			double Current_cost = 0;
			srand((unsigned)time(0));
			RandStep = (int)(rand() % (points_in_time.size() + 1));
			while (RandStep > points_in_time.size() || RandStep == points_in_time.size() || RandStep < 0 || RandStep == 0)
			{
				RandStep = (int)(rand() % (points_in_time.size() + 1));
			}
			for (int i = 0; i < 3; i++)
			{
				Current_cost += pow(cryst_degree.at(RandStep), 2);
				int nextRandStep = RandStep + (int)(rand() % (points_in_time.size() + 1));
				int backRandStep = RandStep - (int)(rand() % (points_in_time.size() + 1));
				if (nextRandStep < points_in_time.size())
					RandStep = nextRandStep;
				else
					if (backRandStep > 0)
						RandStep = backRandStep;
			}*/
			optimisation_quality = Cost_Function(points_in_time, cryst_degree, target_value_file);
			if (optimization_.no_iteration == 0)
			{
				//calculate Current from target_file_value
				optimization_.Best_Cost = optimisation_quality;

			}
			return optimisation_quality;
		}

		SphaeroSim::Material* Optimization::OptimiseMaterial(const double quality_value, SphaeroSim::Material* material_)
		{
			//for now the number of iteration of algorithm should be 3
			if (optimization_.no_iteration > 3 || optimization_.SA_T < 0.00008)
				return nullptr;

			//check the number of adjacent of v0 created
			if (optimization_.n_adjacent_v0 == no_adjacent)
			{
				optimization_.no_iteration++;
				//SA_T = SA_T_Last * exp(i * ln(alpha));
				optimization_.SA_T *= alpha;

				optimization_.n_adjacent_v0 = 0;
				//	continue;
			}
			double propability = exp((quality_value - optimization_.Best_Cost) / optimization_.SA_T);
			double randprobability = (double)(rand() % 2);
			if (propability > randprobability)
			{
				optimization_.Last_V0 = optimization_.Result_V0;
				//create new v0 for the new simulation
				optimization_.Result_V0 = NormalRand(optimization_.Last_V0, sigma);
				//change the value of v0 (c_ath) in the xml file as input file for the simulation
				ChangeCathValue(optimization_.Result_V0, optimization_.GetParameterFile());

				optimization_.Best_Cost = quality_value;
				optimization_.n_adjacent_v0++;
				//check kon
				return  material_;
			}
		}

		void Optimization::ChangeCathValue(double V0, const std::string& parameter_file_name) {

			//ebteda inja ye copy az parameter_file begir va dar mahale zakhire khorooji barname gharar bede


			tinyxml2::XMLDocument file_handle(true, tinyxml2::PRESERVE_WHITESPACE);
			if (file_handle.LoadFile(parameter_file_name.c_str()) != tinyxml2::XML_NO_ERROR) {
				throw SphaeroSim::Exception("Parameter file",
					"ParameterFile is not found.");
			}

			// load the root entry
			tinyxml2::XMLElement* root_element =
				file_handle.FirstChildElement("SphaeroSim");
			if (root_element == NULL)
				throw SphaeroSim::Exception("Value missing in parameter file",
					"The value <SphaeroSim> is missing.");

			// Simulation
			//tinyxml2::XMLElement* cur_element
			tinyxml2::XMLElement* cur_element =
				root_element->FirstChildElement("Simulation");
			if (cur_element == NULL)
				throw  SphaeroSim::Exception("Value missing in parameter file", "At least one <Simulation> must be defined.");


			//NucleationModel
			tinyxml2::XMLElement* Nucleationmodel_element =
				cur_element->FirstChildElement("Nucleationmodel");
			if (Nucleationmodel_element == NULL)
				throw SphaeroSim::Exception("Value missing in parameter file", "At least one <NucleationModel> must be defined.");

			//change C_ath
			tinyxml2::XMLElement* C_ath_element =
				Nucleationmodel_element->FirstChildElement("C_ath");
			if (C_ath_element == NULL)
				throw SphaeroSim::Exception("Value missing in parameter file", "At least one <C_ath> must be defined.");
			Nucleationmodel_element->DeleteAttribute("C_ath");
			Nucleationmodel_element->SetAttribute("C_ath", V0);
		}
	};

}