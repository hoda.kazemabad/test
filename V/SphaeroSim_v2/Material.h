#ifndef _SPHAEROSIM_MATERIAL_H_
#define _SPHAEROSIM_MATERIAL_H_

#ifndef _INCLUDED_STRING_H_
#include <string>
#define _INCLUDED_STRING_H_
#endif

#ifndef _SPHAEROSIM_DISENGAGEMENT_TIME_H_
#include "DisengagementTime.h"
#endif

#ifndef _SPHAEROSIM_NUCLEATION_MODEL_H_
#include "NucleationModel.h"
#endif

#ifndef _SPHAEROSIM_GROWTH_MODEL_H_
#include "GrowthModel.h"
#endif

namespace SphaeroSim {

class Material {
 public:
  Material(const std::string& name,
           const double density,
           const double T_m0,
           const double T_inf,
           const double mol_weight_ent,
           const double enthalpy,
           DisengagementTime* disengagement_time,
		   GrowthModel* growth_model,
		   NucleationModel* nucleation_model) :
    name_(name),
    density_(density), 
    T_m0_(T_m0),
    T_inf_(T_inf),
    mol_weight_ent_(mol_weight_ent),
    enthalpy_(enthalpy) {
    disengagement_time_ = disengagement_time;
	growth_model_ = growth_model;
	nucleation_model_ = nucleation_model;
  }

  virtual ~Material() {
    if (disengagement_time_ != NULL)
      delete disengagement_time_;
	if (growth_model_ != NULL)
		delete growth_model_;
	if (nucleation_model_ != NULL)
		delete nucleation_model_;
  }

  inline void SetNucleationModel(NucleationModel* new_model) {
	  nucleation_model_ = new_model;
  }
  inline void SetGrowthModel(GrowthModel* new_model) {
	  growth_model_ = new_model;
  }

  // getter
  inline const std::string GetName() const {
    return name_;
  }
  inline const double GetDensity() const {
    return density_;
  }
  inline const double GetTm0() const {
    return T_m0_;
  }
  inline const double GetTinf() const {
    return T_inf_;
  }
  inline const double GetEnthalpy() const {
    return enthalpy_;
  }
  inline const double GetMolWeightEnt() const {
    return mol_weight_ent_;
  }
  inline const double GetDisengagementTime(const double temperature) const {
    return disengagement_time_->Calculate(temperature,
                                          GetDensity(),
                                          GetMolWeightEnt() / 1000.0);
  }
  inline const double GetDisengagementTimeDT(const double temperature) const {
    temperature;
    return 0.0;
  }
  inline GrowthModel* GetGrowthModel() const {
	  return growth_model_;
  }
  inline NucleationModel* GetNucleationModel() const {
	  return nucleation_model_;
  }
 private:
  const std::string name_;
  const double density_;
  const double T_m0_;
  const double T_inf_;
  const double enthalpy_;
  const double mol_weight_ent_;
  DisengagementTime* disengagement_time_;
  GrowthModel* growth_model_;
  NucleationModel* nucleation_model_;

  Material();
  Material& operator=(const Material&);
};

}

#endif  // _SPHAEROSIM_MATERIAL_H_
