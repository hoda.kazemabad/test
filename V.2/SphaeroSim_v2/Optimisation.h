#ifndef __OPTIMISATION_H_
#define __OPTIMISATION_H_

#ifndef _INCLUDED_STRING_H_
#include <string>
#define _INCLUDED_STRING_H_
#endif

#ifndef _INCLUDED_SSTREAM_H_
#include <sstream>
#define _INCLUDED_SSTREAM_H_
#endif

#ifndef _INCLUDED_VECTOR_H_
#include <vector>
#define _INCLUDED_VECTOR_H_
#endif

#ifndef _INCLUDED_CTIME_H_
#include <ctime>
#define _INCLUDED_CTIME_H_
#endif

#ifndef _SPHAEROSIM_MATERIAL_H_
#include "Material.h"
#endif

#ifndef _SPHAEROSIM_EIGEN_LIBRARY_H_
#include "EigenLibrary.h"
#endif

#ifndef _SPHAEROSIM_EXCEPTION_H_
#include "Exception.h"
#endif

#ifndef _SPHAEROSIM_INTERPOLATION_H_
#include "Interpolation.h"
#endif

#ifndef _SPHAEROSIM_SIMULATION_H_
#include "Simulation.h"
#endif



#include <time.h>


//DEFUALT VARIABLES
#define PI 3.14159265
#define sigma 0.1
#define no_step 3
#define alpha 0/99
#define no_adjacent 3
#define _c_ath_ 1.5e-28
#define max_C_ath_ _c_ath_ + 1
#define min_C_ath_ _c_ath_ - 1

namespace SphaeroSim {

	class Optimization
	{
	public:
		Optimization();
		~Optimization();

		std::vector<std::vector<double>> ReadfromExcel(std::string CrysDeg_PointTime);

		double NormalRand(double V0,
			double Zigma);

		double Cost_Function(int countstep,
			std::vector<double> cryst_degree, std::string target_value_file);
		
		SphaeroSim::Material* OptimiseMaterial(const double quality_value,
			const SphaeroSim::Material* Material);


		void ReplaceMaterial(SphaeroSim::Material* optimised_material) {
			optimised_material_ = optimised_material;
		}

		void ChangeCathValue(double V0, 
			std::string& parameter_file);

		inline std::string GetParameterFile() {
			return Parameter_file_;
		}
		const std::size_t GetProbeID() const;

		const std::string GetTargetValueFile() const;

		void GetElementProbeResults(std::string probe_Name,
			std::string filename_str,
			std::vector<double>* points_in_time,
			std::vector<double>* cryst_degree);

		const double GetOptimisationQuality(const std::string target_value_file,
			std::vector<double> points_in_time,
			std::vector<double> cryst_degree);

		inline void SetParameterFile(const std::string Parameter_file) {
			Parameter_file_ = Parameter_file;
		}

		size_t no_iteration = 0, probe_id;
		double Best_Cost, Result_V0 = _c_ath_, Last_V0;
		int SA_T = 120, no_result = 1, n_adjacent_v0 = 0;
		int CountStep;

	protected:
		SphaeroSim::Material* optimised_material_;
		std::vector<std::string> RelCrysDeg, output_RelCrystDegree;
		std::string Parameter_file_;

		

	} optimization_;

}

#endif  // _SPHAEROSIM_APPLICATION_H_